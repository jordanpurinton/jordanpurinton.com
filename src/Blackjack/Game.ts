import type { Deck } from './Deck';
import type { Player as PlayerType } from './Player';
import { Player } from './Player';

const BLACKJACK_SCORE = 21;

export enum Result {
  None,
  PlayerWin,
  DealerWin,
  Tie,
}

export enum GameStatus {
  InProgress = 'InProgress',
  EnteringUsername = 'EnteringUsername',
  GameOver = 'GameOver',
  GameNotStarted = 'GameNotStarted',
}

export class Game {
  private _result: Result = Result.None;
  private _gameStatus: GameStatus = GameStatus.GameNotStarted;
  private _isPendingPlayerMove = false;
  private _isPendingDealerMove = false;
  constructor(
    public deck: Deck,
    public player: PlayerType,
    public dealer: PlayerType
  ) {
    this.deck = deck;
    this.player = player;
    this.dealer = dealer;
  }

  public get result(): Result {
    return this._result;
  }
  public set result(newResult: Result) {
    this._result = newResult;
  }

  public get gameStatus(): GameStatus {
    return this._gameStatus;
  }
  public set gameStatus(newGameStatus: GameStatus) {
    this._gameStatus = newGameStatus;
  }

  public get isPendingPlayerMove(): boolean {
    return this._isPendingPlayerMove;
  }
  public set isPendingPlayerMove(isPendingPlayerMoveBool) {
    this._isPendingPlayerMove = isPendingPlayerMoveBool;
  }

  public get isPendingDealerMove(): boolean {
    return this._isPendingDealerMove;
  }
  public set isPendingDealerMove(isPendingDealerMoveBool) {
    this._isPendingDealerMove = isPendingDealerMoveBool;
  }

  public createPlayer(playerName: string) {
    this.player = new Player(playerName);
    this.player.drawCard(this.deck);
    this.dealer.drawCard(this.deck);
    this.gameStatus = GameStatus.InProgress;
  }

  public drawPlayerCard() {
    this.player.drawCard(this.deck);
    this.isPendingPlayerMove = false;

    if (!this.dealer.isStanding) {
      this.isPendingDealerMove = true;
    } else {
      this.isPendingPlayerMove = true;
    }

    // handle ties
    if (
      this.player.score === this.dealer.score &&
      this.player.isStanding &&
      this.dealer.isStanding
    ) {
      this.result = Result.Tie;
      this.gameStatus = GameStatus.GameOver;
      return;
    }

    // automatically stand when player gets a 21
    if (this.player.score === BLACKJACK_SCORE) {
      this.player.isStanding = true;
      if (this.dealer.isStanding && this.player.score > this.dealer.score) {
        this.result = Result.PlayerWin;
        this.gameStatus = GameStatus.GameOver;
      }
      return;
    }

    // dealer stood, player has higher score, player wins
    if (
      this.dealer.isStanding &&
      this.player.score > this.dealer.score &&
      this.player.score <= BLACKJACK_SCORE
    ) {
      this.result = Result.PlayerWin;
      this.gameStatus = GameStatus.GameOver;
      return;
    }

    // dealer busted, dealer win
    if (this.player.score > BLACKJACK_SCORE) {
      this.player.didBust = true;
      this.result = Result.DealerWin;
      this.gameStatus = GameStatus.GameOver;
      return;
    }
  }

  public drawDealerCard() {
    this.dealer.drawCard(this.deck);
    this.isPendingDealerMove = false;

    if (!this.player.isStanding) {
      this.isPendingPlayerMove = true;
    } else {
      this.isPendingDealerMove = true;
    }

    // handle ties
    if (
      this.player.score === this.dealer.score &&
      this.player.isStanding &&
      this.dealer.isStanding
    ) {
      this.result = Result.Tie;
      this.gameStatus = GameStatus.GameOver;
      return;
    }

    // dealer stood, player has higher score, player wins
    if (
      this.player.isStanding &&
      this.dealer.score > this.player.score &&
      this.dealer.score <= BLACKJACK_SCORE
    ) {
      this.result = Result.DealerWin;
      this.gameStatus = GameStatus.GameOver;
      return;
    }

    // dealer busted, player win
    if (this.dealer.score > BLACKJACK_SCORE) {
      this.dealer.didBust = true;
      this.result = Result.PlayerWin;
      this.gameStatus = GameStatus.GameOver;
      return;
    }

    // stand if at 17 or higher and less than 22
    if (
      this.dealer.score >= 17 &&
      this.dealer.score < BLACKJACK_SCORE + 1 &&
      this.player.score < this.dealer.score
    ) {
      this.dealer.isStanding = true;
      if (this.player.isStanding && this.dealer.score > this.player.score) {
        this.result = Result.DealerWin;
        this.gameStatus = GameStatus.GameOver;
      }
      return;
    }
  }
}
