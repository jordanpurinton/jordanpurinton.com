import type { Deck } from './Deck';
import type { Card } from './Card';
export class Player {
  private _score = 0;
  private _currentCards: Card[] = [];
  private _isStanding = false;
  private _didBust = false;
  constructor(public name: string) {
    this.name = name;
  }

  public get score(): number {
    return this._score;
  }
  public set score(newScore: number) {
    this._score = newScore;
  }

  public get currentCards(): Card[] {
    return this._currentCards;
  }
  public set currentCards(newCurrentCards: Card[]) {
    this._currentCards = newCurrentCards;
  }

  public get isStanding(): boolean {
    return this._isStanding;
  }
  public set isStanding(isStandingBool) {
    this._isStanding = isStandingBool;
  }

  public get didBust(): boolean {
    return this._didBust;
  }
  public set didBust(didBustBool) {
    this._didBust = didBustBool;
  }

  public listCurrentHand(): string[] {
    return this._currentCards.map(
      (card: Card) => ` ${card.name} of ${card.suit} `
    );
  }

  drawCard(currentDeck: Deck): void {
    const { cards } = currentDeck;
    const drawnCardIndex = Math.floor(Math.random() * cards.length);
    const drawnCard = cards[drawnCardIndex];
    currentDeck.cards.splice(drawnCardIndex, 1);
    this._currentCards.push(drawnCard);
    this.updateScore(drawnCard);
  }

  updateScore(card: Card): void {
    // todo: update logic for ace here
    this._score += typeof card.value === 'number' ? card.value : card.value[0];
  }
}
