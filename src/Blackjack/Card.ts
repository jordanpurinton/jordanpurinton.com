export type Suit = '♢' | '♧' | '♡' | '♤';
export type CardValue = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11;
export enum Suits {
  Diamonds = '♢',
  Spades = '♤',
  Hearts = '♡',
  Clubs = '♧',
}
export enum CardName {
  'Ace',
  'One',
  'Two',
  'Three',
  'Four',
  'Five',
  'Six',
  'Seven',
  'Eight',
  'Nine',
  'Ten',
  'Jack',
  'Queen',
  'King',
}

export class Card {
  name: CardName;
  value: CardValue | number[];
  suit: Suit;
  constructor(name: CardName, value: CardValue | number[], suit: Suit) {
      this.name = name;
      this.value = value;
      this.suit = suit;
  }
}
